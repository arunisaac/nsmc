;;; nsmc --- n-sphere Monte Carlo method
;;; Copyright © 2021 Arun I <arunisaac@systemreboot.net>
;;; Copyright © 2021 Murugesan Venkatapathi <murugesh@iisc.ac.in>
;;;
;;; This file is part of nsmc.
;;;
;;; nsmc is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; nsmc is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with nsmc.  If not, see <https://www.gnu.org/licenses/>.

(pre-include "gsl/gsl_math.h")
(pre-include "gsl/gsl_poly.h")
(pre-include "integrands.h")

(define (polynomial-integrand r x -params) (double double (const gsl-vector*) void*)
  (let* ((params polynomial-integrand-params*
                 (convert-type -params polynomial-integrand-params*)))
    (return (gsl-poly-eval (: params coefficients)
                           (+ (: params degree) 1)
                           r))))

(define (gaussian-integrand r x -params) (double double (const gsl-vector*) void*)
  (return (exp (- (/ (gsl-pow-2 r) 2)))))

(define (x-coordinate-integrand r x -params) (double double (const gsl-vector*) void*)
  (return (fabs (* r (gsl-vector-get x 0)))))
