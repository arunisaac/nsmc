#include <nd-random.h>

// Dimension of the vector to generate
#define DIMENSION 10
// Minimum angle from the axis of the cone
#define THETA_MIN M_PI/4
// Maximum angle from the axis of the cone
#define THETA_MAX M_PI/3

int main ()
{
  // Initialize random number generator.
  gsl_rng_env_setup();
  gsl_rng* r = gsl_rng_alloc(gsl_rng_default);
  // Allocate vector to be generated.
  gsl_vector *x = gsl_vector_alloc(DIMENSION);
  // Allocate and initialize central axis of cone.
  gsl_vector *axis = gsl_vector_alloc(DIMENSION);
  gsl_vector_set_all(axis, 1/sqrt(DIMENSION));

  // Generate random vector on spherical cap of hollow cone.
  hollow_cone_random_vector(r, axis, THETA_MIN, THETA_MAX, x);

  // Print out generated vector.
  gsl_vector_fprintf(stdout, x, "%g");

  // Free allocated memory.
  gsl_vector_free(axis);
  gsl_vector_free(x);
  gsl_rng_free(r);
  return 0;
}
