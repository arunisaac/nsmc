/*
  nsmc --- n-sphere Monte Carlo method
  Copyright © 2021 Arun I <arunisaac@systemreboot.net>
  Copyright © 2021 Murugesan Venkatapathi <murugesh@iisc.ac.in>

  This file is part of nsmc.

  nsmc is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  nsmc is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with nsmc.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ORACLES_H
#define ORACLES_H

#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>

typedef struct {
  double p, r0, r1;
} bernoulli_params;

double bernoulli_extent_oracle (const gsl_rng *r, const gsl_vector *x, void *params);
double bernoulli_true_volume (unsigned int dimension, void *params);

typedef struct {
  double a, b;
} uniform_params;

double uniform_extent_oracle (const gsl_rng *r, const gsl_vector *x, void *params);
double uniform_true_volume (unsigned int dimension, void *params);

typedef struct {
  double alpha, beta;
} beta_params;

double beta_extent_oracle (const gsl_rng *r, const gsl_vector *x, void *params);
double beta_true_volume (unsigned int dimension, void *params);

typedef struct {
  double edge;
  const gsl_vector* center;
} cube_params;

double cube_extent_oracle (const gsl_rng *r, const gsl_vector *x, void *params);
double cube_extent_oracle_with_center (const gsl_rng *r, const gsl_vector *x, void *params);
double cube_true_volume (unsigned int dimension, void *params);

typedef struct {
  const gsl_vector* axes;
} ellipsoid_params;

double ellipsoid_extent_oracle (const gsl_rng *r, const gsl_vector *x, void *params);
double ellipsoid_true_volume (unsigned int dimension, void *params);

typedef struct {
  double eccentricity;
} spheroid_params;

double spheroid_extent_oracle (const gsl_rng *r, const gsl_vector *x, void *params);
double spheroid_true_volume (unsigned int dimension, void *params);

#endif
