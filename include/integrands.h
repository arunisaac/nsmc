/*
  nsmc --- n-sphere Monte Carlo method
  Copyright © 2021 Arun I <arunisaac@systemreboot.net>
  Copyright © 2021 Murugesan Venkatapathi <murugesh@iisc.ac.in>

  This file is part of nsmc.

  nsmc is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  nsmc is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with nsmc.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef INTEGRANDS_H
#define INTEGRANDS_H

#include <gsl/gsl_vector.h>

typedef struct {
  double *coefficients;
  int degree;
} polynomial_integrand_params;

double polynomial_integrand (double r, const gsl_vector* x, void *params);
double gaussian_integrand (double r, const gsl_vector* x, void *params);
double x_coordinate_integrand (double r, const gsl_vector* x, void *params);

#endif
