/*
  nsmc --- n-sphere Monte Carlo method
  Copyright © 2021 Arun I <arunisaac@systemreboot.net>
  Copyright © 2021 Murugesan Venkatapathi <murugesh@iisc.ac.in>

  This file is part of nsmc.

  nsmc is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  nsmc is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with nsmc.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UTILS_H
#define UTILS_H

#include <gsl/gsl_roots.h>
#include <gsl/gsl_vector.h>

#define SIGNUM(x) ((x) < 0 ? -1 : 1)

double volume_of_ball (unsigned int dimension);
double ln_volume_of_ball (unsigned int dimension);
double surface_area_of_ball (unsigned int dimension);
double ln_surface_area_of_ball (unsigned int dimension);

double angle_between_vectors (const gsl_vector* x, const gsl_vector* y);
double dot_product (const gsl_vector* x, const gsl_vector* y);
double gaussian_pdf (double x);
double gaussian_cdf (double x);

double rerror (double approx, double exact);

double bisection (gsl_function* f, double a, double b);
double bisection_rlimit (gsl_function* f, double a, double b);

#endif
