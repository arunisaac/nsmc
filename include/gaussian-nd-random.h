/*
  nsmc --- n-sphere Monte Carlo method
  Copyright © 2021 Arun I <arunisaac@systemreboot.net>
  Copyright © 2021 Murugesan Venkatapathi <murugesh@iisc.ac.in>

  This file is part of nsmc.

  nsmc is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  nsmc is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with nsmc.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GAUSSIAN_ND_RANDOM_H
#define GAUSSIAN_ND_RANDOM_H

#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_integration.h>

double planar_angle_to_standard_deviation
(double mean, double theta_max, double truncation, unsigned int dimension);

unsigned int shifted_gaussian_random_vector
(const gsl_rng* r, const gsl_vector* mean,
 double theta_max, double truncation, gsl_vector* x);

double shifted_gaussian_pdf
(double theta, double mean, double theta_max,
 double truncation, unsigned int dimension, gsl_integration_workspace* w);

#endif
