/*
  nsmc --- n-sphere Monte Carlo method
  Copyright © 2021 Arun I <arunisaac@systemreboot.net>
  Copyright © 2021 Murugesan Venkatapathi <murugesh@iisc.ac.in>

  This file is part of nsmc.

  nsmc is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  nsmc is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with nsmc.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EXTENT_SAMPLING_H
#define EXTENT_SAMPLING_H

#include <gsl/gsl_rng.h>
#include <gsl/gsl_rstat.h>
#include <gsl/gsl_vector.h>

typedef struct {
  double (*oracle) (const gsl_rng*, const gsl_vector*, void*);
  void *params;
} extent_oracle_t;

typedef struct {
  double (*integrand) (double, const gsl_vector*, void*);
  void *params;
} integrand_t;

void init_random (void);

double volume
(extent_oracle_t *extent_oracle, double true_volume,
 const gsl_rng* r, unsigned int dimension, double rtol,
 gsl_rstat_workspace* stats);

double volume_window
(extent_oracle_t *extent_oracle, double true_volume,
 const gsl_rng* r, unsigned int dimension, double rtol,
 unsigned int* number_of_samples);

double integral
(integrand_t *integrand, extent_oracle_t *extent_oracle, double true_integral,
 const gsl_rng* r, unsigned int dimension, double rtol,
 gsl_rstat_workspace* stats);

double volume_cone
(extent_oracle_t *extent_oracle, const gsl_rng* r,
 const gsl_vector* mean, double omega_min, double omega_max,
 unsigned int number_of_samples, double* variance);

double volume_experiment
(extent_oracle_t *extent_oracle, const gsl_rng* r,
 const gsl_vector* mean, unsigned int samples_per_cone,
 double solid_angle_factor, double solid_angle_threshold_exponent_factor,
 unsigned int* number_of_samples);

#endif
