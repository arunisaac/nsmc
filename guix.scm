;;; Run the following command to enter a development environment for
;;; nsmc:
;;;
;;;  $ guix environment -l guix.scm

(use-modules (ice-9 match)
             (ice-9 popen)
             (ice-9 rdelim)
             (srfi srfi-1)
             (srfi srfi-26)
             (guix build utils)
             (guix build-system cmake)
             (guix build-system guile)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (guix utils)
             (gnu packages bash)
             (gnu packages code)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages maths)
             (gnu packages pkg-config))

(define %source-dir (dirname (current-filename)))

(define git-file?
  (let* ((pipe (with-directory-excursion %source-dir
                 (open-pipe* OPEN_READ "git" "ls-files")))
         (files (let loop ((lines '()))
                  (match (read-line pipe)
                    ((? eof-object?)
                     (reverse lines))
                    (line
                     (loop (cons line lines))))))
         (status (close-pipe pipe)))
    (lambda (file stat)
      (match (stat:type stat)
        ('directory #t)
        ((or 'regular 'symlink)
         (any (cut string-suffix? <> file) files))
        (_ #f)))))

;; TODO: Contribute upstream to Guix and delete.
(define sph-lib
  (package
    (name "sph-lib")
    (version "2021-01-05")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/sph-mn/sph-lib")
             (commit "2b0474218e51c1debb4de7b932a0e1c8bff4577c")))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0yz8igkv725ddjlss4jhzzwam3p4zkw38z45pydsdv4q912qjxml"))))
    (build-system guile-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-build-errors
           (lambda _
             (substitute* "modules/sph/documentation.scm"
               (("\\(define output-format-markdown" all)
                (string-append "(define default-format-arguments)\n" all)))
             (chdir "modules")
             (delete-file "sph/documentation/itpn.scm")
             (delete-file-recursively "test"))))))
    (inputs
     `(("guile" ,guile-3.0)
       ("guile-fibers" ,guile-fibers)
       ("guile-reader" ,guile-reader)))
    (home-page "https://github.com/sph-mn/sph-lib")
    (synopsis "Collection of guile scheme libraries")
    (description "Collection of guile scheme libraries")
    (license license:gpl3+)))

;; TODO: Contribute upstream to Guix and delete.
(define sph-sc
  (package
    (name "sph-sc")
    (version "2021-02-02")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/sph-mn/sph-sc")
             (commit "42c992737a5a1a176706c9a3c30c32ca3828476c")))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0f3nssnqpqbm5wfv53lg8wi3dcpc0l9qyysd9r1bvig343gs79lm"))))
    (build-system guile-build-system)
    (inputs
     `(("bash" ,bash)
       ("guile" ,guile-3.0)
       ("sph-lib" ,sph-lib)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'build 'fix-build-errors
           (lambda* (#:key outputs #:allow-other-keys)
             (copy-recursively "other/sc-format/modules" "modules")
             (chdir "modules")
             (delete-file-recursively "test")))
         (add-after 'install 'install-executables
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (string-append (assoc-ref outputs "out")))
                    (bin (string-append out "/bin"))
                    (version ,(version-major+minor (package-version guile-3.0)))
                    (scm (string-append out "/share/guile/site/" version))
                    (go (string-append out "/share/guile/" version "/site-ccache")))
               (mkdir bin)
               (for-each (lambda (executable)
                           (let ((installed-executable (string-append bin "/" (basename executable))))
                             (copy-file executable installed-executable)
                             (wrap-program installed-executable
                               `("GUILE_LOAD_PATH" ":" prefix
                                 (,scm ,(getenv "GUILE_LOAD_PATH")))
                               `("GUILE_LOAD_COMPILED_PATH" ":" prefix
                                 (,go ,(getenv "GUILE_LOAD_COMPILED_PATH"))))))
                         (list "../exe/sc"
                               "../other/sc-format/exe/sc-format"))))))))
    (home-page "https://github.com/sph-mn/sph-sc")
    (synopsis "Compile scheme-like S-expressions to C")
    (description "Compile scheme-like S-expressions to C")
    (license license:gpl3+)))

(package
  (name "nsmc")
  (version "0.1.0")
  (home-page "https://git.systemreboot.net/nsmc")
  (source (local-file %source-dir
                      #:recursive? #t
                      #:select? git-file?))
  (build-system cmake-build-system)
  (arguments
   '(#:tests? #f ; no tests
     #:make-flags '("GUILE_AUTO_COMPILE=0"))) ; to prevent guild warnings
  (inputs
   `(("gsl" ,gsl)
     ("guile" ,guile-3.0)))
  (native-inputs
   `(("indent" ,indent) ; indent is really optional, but it indents
                        ; the generated C code making it
                        ; readable. That can be useful.
     ("pkg-config" ,pkg-config)
     ("sph-sc" ,sph-sc)))
  (synopsis "n-sphere Monte Carlo library")
  (description "n-sphere Monte Carlo library")
  (license license:gpl3+))
